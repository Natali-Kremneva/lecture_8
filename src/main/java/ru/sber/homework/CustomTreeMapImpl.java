package ru.sber.homework;

import java.util.*;

public class CustomTreeMapImpl <K, V> implements CustomTreeMap<K, V> {

    private Comparator<K> comparator;
    private Node<K, V> root;
    private int size;

    public CustomTreeMapImpl(Comparator<K> comparator) {
        this.comparator = comparator;
    }

    private class Node<K,V> {

        private K key;
        private V value;
        private Node<K, V> left;
        private Node<K, V> right;
        private Node<K, V> parent;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public void setLeftChild(Node<K, V> node) {

            left = node;

            if (left != null) {
                left.parent = this;
            }
        }

        public void setRightChild(Node<K, V> node) {

            right = node;

            if (right != null) {
                right.parent = this;
            }
        }

    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public V get(K key) {
        if (root == null){
            return null;
        }
        Node<K, V> foundItem = findNodeByKey(root, key);
        if(Objects.nonNull(foundItem)){
            return foundItem.value;
        } else {
            return null;
        }
    }

    @Override
    public V put(K key, V value) {
        if(root==null){
            root = new Node<>(key, value);
            ++size;
            return null;
        }

        V oldValue = insert(root, key, value);
        return oldValue;
    }

    private V insert(Node<K,V> node, K key, V value) {
        int cmpResult = comparator.compare(key, node.key);

        if(cmpResult==0){
            V oldValue = node.value;
            node.value = value;
            return oldValue;
        }

        if(cmpResult<0 && node.left==null){
            ++size;
            Node<K,V> newNode = new Node<>(key, value);
            node.setLeftChild(newNode);
            return null;
        }

        if(cmpResult>0 && node.right==null){
            ++size;
            Node<K,V> newNode = new Node<>(key, value);
            node.setRightChild(newNode);
            return null;
        }
        if(cmpResult<0){
            return insert(node.left, key, value);
        } else {
            return insert(node.right, key, value);
        }
    }

    @Override
    public V remove(K key) {

        if (root == null) {
            return null;
        }
        Node<K,V> x = findNodeByKey(root, key);

        if (x == null) {
            return null;
        }
        if(x==root){
            if(x.right==null&& x.left == null) {
                root = null;
            }else if(x.right!=null&& x.left == null) {
                root = x.right;
            }else if(x.right==null&& x.left != null) {
                root = x.left;
            } else {
                x.right.setLeftChild(root.left);
                root = x.right;
            }
        } else {
            int result = comparator.compare(root.key, x.key);
            if (x.right == null) {
                if (x.left != null) {
                    x.parent.setRightChild(x.left);
                } else {

                    if (comparator.compare(x.key, x.parent.key) < 0) {
                        x.parent.left = null;
                    }
                    if (comparator.compare(x.key, x.parent.key) > 0)
                        x.parent.right = null;
                }
            } else if (x.right != null && x.right.left == null) {

                x.right.setLeftChild(x.left);
                if (result == 1) {
                    x.parent.setLeftChild(x.right);
                } else {
                    x.parent.setRightChild(x.right);
                }

            } else if (x.right != null && x.right.left != null) {
                Node<K,V> y = x.right;
                Node<K,V> z = y;
                while (z.left != null) {
                    z = z.left;
                }

                if (z.right != null) {
                    z.parent.setLeftChild(z.right);

                } else {
                    z.parent.left = null;
                }
                if (result == 1) {
                    x.parent.setLeftChild(z);

                } else {
                    x.parent.setRightChild(z);
                }
                if (x.left != null) {
                    z.setLeftChild(x.left);
                }
                z.setRightChild(y);
            }
        }
        size--;
        return x.value;
    }

    private Node<K, V> findNodeByKey(Node<K, V> node, K key) {

        int result = comparator.compare(key, node.key);

        if (result == 0) {
            return node;
        }

        Node<K, V> searchIn = result < 0 ? node.left : node.right;
        if (searchIn == null) {
            return null;
        }
        return findNodeByKey(searchIn, key);
    }

    @Override
    public boolean containsKey(K key) {
        if (root == null){
            return false;
        }
        Node<K, V> foundItem = findNodeByKey(root, key);
        return foundItem != null;
    }

    @Override
    public boolean containsValue(V value) {
        Node<K,V> node = root;
        List<V> map = new ArrayList<>();
        if(root != null) {
            getValueTree(map, node);
        }
        for (int i = 0; i < size(); i++) {
            if(map.get(i).equals(value)){
                return true;
            }
        }
        return false;
    }

    @Override
    public Object[] keys() {
        Object[] objects = new Object[size()];
        Node<K,V> node = root;
        List<K> map = new ArrayList<>();
        if(root != null) {
            getKyeTree(map, node);
        }
        for (int i = 0; i < size(); i++) {
            objects[i]=map.get(i);
        }
        return objects;
    }

    @Override
    public Object[] values() {
        Object[] objects = new Object[size()];
        Node<K,V> node = root;
        List<V> map = new ArrayList<>();
        if(root != null) {
            getValueTree(map, node);
        }
        for (int i = 0; i < size(); i++) {
            objects[i]=map.get(i);
        }
        return objects;
    }


    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append("[ ");

        if(Objects.nonNull(root)){
            printTreeRecursive(root, sb);
        }

        sb.append(" ]");

        return sb.toString();
    }

    private void printTreeRecursive(Node<K,V> node, StringBuilder sb) {

        if(Objects.nonNull(node.left)){
            printTreeRecursive(node.left, sb);
        }
        sb.append("{key=" + node.key + "; value=" + node.value + "} ");
        if(Objects.nonNull(node.right)){
            printTreeRecursive(node.right, sb);
        }
    }
    private void getKyeTree(List<K> map, Node<K,V> node) {

        if (node.left != null) {
            getKyeTree(map,node.left);
        }
        map.add(node.key);

        if (node.right != null) {
            getKyeTree(map,node.right);
        }
    }


    private void getValueTree(List<V> map, Node<K,V> node) {
        if (node.left != null) {
            getValueTree(map,node.left);
        }
        map.add(node.value);

        if (node.right != null) {
            getValueTree(map,node.right);
        }
    }
}
