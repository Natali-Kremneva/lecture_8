package ru.sber.homework;

import org.junit.Assert;
import org.junit.Test;

public class CustomTreeMapImplTest {

    @Test
    public void size() {
        CustomTreeMapImpl<Integer, String> map = new CustomTreeMapImpl<>(Integer::compareTo);

        Assert.assertEquals(0, map.size());
    }

    @Test
    public void isEmpty() {
        CustomTreeMapImpl<Integer, String> map = new CustomTreeMapImpl<>(Integer::compareTo);

        Assert.assertTrue(map.isEmpty());
    }

    @Test
    public void get() {
        CustomTreeMapImpl<Integer, String> map = new CustomTreeMapImpl<>(Integer::compareTo);
        map.put(1, "Elem_1");
        map.put(4, "Elem_4");
        map.put(5, "Elem_5");
        map.put(2, "Elem_2");
        map.put(4, "New_Elem_4");
        map.put(3, "Elem_3");
        System.out.println(map);
        Assert.assertEquals(5, map.size());
        System.out.println(map.get(4));
        System.out.println(map.get(7));
        Assert.assertEquals("Elem_2", map.get(2));
    }

    @Test
    public void put() {
        CustomTreeMapImpl<Integer, String> map = new CustomTreeMapImpl<>(Integer::compareTo);
        map.put(1, "Elem_1");
        map.put(4, "Elem_4");
        map.put(5, "Elem_5");
        map.put(2, "Elem_2");
        map.put(4, "New_Elem_4");
        map.put(3, "Elem_3");
        System.out.println(map);
        Assert.assertEquals(5, map.size());
    }

    @Test
    public void remove() {
        CustomTreeMapImpl<Integer, String> map = new CustomTreeMapImpl<>(Integer::compareTo);

        map.put(30, "");

        map.put(50,"");
        map.put(10,"");

        map.put(5,"");
        map.put(20,"");
        map.put(70,"");
        map.put(100,"");
        map.put(60,"");
        map.put(65,"");
        map.put(55,"");
        map.put(56,"");
        map.put(19,"");
        System.out.println(map);


        map.remove(20);
        System.out.println(map);
        map.remove(100);
        System.out.println(map);


        map.remove(65);
        System.out.println(map);


        map.remove(50);
        System.out.println(map);

        map.remove(30);
        System.out.println(map);


        map.remove(5);
        System.out.println(map);
        map.remove(10);
        System.out.println(map);
        map.remove(70);
        System.out.println(map);
        map.remove(19);
        System.out.println(map);
        map.remove(55);
        System.out.println(map);
        map.remove(56);
        System.out.println(map);
        map.remove(60);
        System.out.println(map);

        Assert.assertEquals(0, map.size());

    }

    @Test
    public void containsKey() {
        CustomTreeMapImpl<Integer, String> map = new CustomTreeMapImpl<>(Integer::compareTo);

        map.put(1, "Elem_1");
        map.put(4, "Elem_4");
        map.put(5, "Elem_5");
        map.put(2, "Elem_2");
        map.put(4, "New_Elem_4");
        map.put(3, "Elem_3");
        System.out.println(map);
        Assert.assertEquals(5, map.size());
        System.out.println(map.containsKey(5));
        System.out.println(map.containsKey(7));
        Assert.assertFalse(map.containsKey(7));
        Assert.assertTrue(map.containsKey(5));
    }

    @Test
    public void containsValue() {
        CustomTreeMapImpl<Integer, String> map = new CustomTreeMapImpl<>(Integer::compareTo);

        map.put(20, "Elem_3");
        map.put(18, "Elem_1");
        map.put(22, "Elem_4");
        map.put(19, "Elem_5");
        map.put(23, "Elem_2");
        map.put(17, "New_Elem_17");
        map.put(21, "New_Elem_21");

        System.out.println(map);

        Assert.assertTrue(map.containsValue("New_Elem_17"));
        Assert.assertTrue(map.containsValue("Elem_5"));
        Assert.assertFalse(map.containsValue("New_Elem"));
        Assert.assertTrue(map.containsValue("Elem_1"));
        Assert.assertTrue(map.containsValue("Elem_2"));
        Assert.assertFalse(map.containsValue("Foo"));
        Assert.assertTrue(map.containsValue("Elem_3"));

    }

    @Test
    public void keys() {
        CustomTreeMapImpl<Integer, String> map = new CustomTreeMapImpl<>(Integer::compareTo);

        map.put(20, "Elem_3");
        map.put(18, "Elem_1");
        map.put(22, "Elem_4");
        map.put(19, "Elem_5");
        map.put(23, "Elem_2");
        map.put(17, "New_Elem_17");
        map.put(21, "New_Elem_21");

        Object[] obj = map.keys();
        for(Object o:obj){
            System.out.print(o + " ");
        }

    }

    @Test
    public void values() {
        CustomTreeMapImpl<Integer, String> map = new CustomTreeMapImpl<>(Integer::compareTo);

        map.put(20, "Elem_3");
        map.put(18, "Elem_1");
        map.put(22, "Elem_4");
        map.put(19, "Elem_5");
        map.put(23, "Elem_2");
        map.put(17, "New_Elem_17");
        map.put(21, "New_Elem_21");

        Object[] obj = map.values();
        for(Object o:obj){
            System.out.print(o + " ");
        }
    }
}